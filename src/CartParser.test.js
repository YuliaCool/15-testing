import CartParser from './CartParser';
const path = require("path");

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
	// Add your unit tests here.
	it('should validate correct data without any errors', () => {
		// given
		const mockReturnCartFromCSV = "Product name,Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1";
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual([]);
	});
	it('should generate error about incorrect header, when one of header has incorrect name', () => {
		// given
		const incorrectHeaderName = "CountOfMoney";
		const mockReturnCartFromCSV = `Product name,${incorrectHeaderName},Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": `Expected header to be named \"Price\" but received ${incorrectHeaderName}.`,
				})
			])
		);
	});
	it('should generate error about incorrect header, when one of header name is empty', () => {
		// given
		const mockReturnCartFromCSV = `Product name,Price\nMollis consequat,9.00,2\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": "Expected header to be named \"Quantity\" but received undefined.",
				})
			])
		);
	});

	it('should generate error about incorrect cell length, when second row has 1 extra cell with number value', () => {
		// given
		const extraNumberValue = "100500";
		const mockReturnCartFromCSV = `Product name,Price,Quantity\nMollis consequat,9.00,${extraNumberValue},2\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": "Expected row to have 3 cells but received 4."
				})
			])
		);
	});

	it('should generate error about empty number value, when second row has second number value as empty string', () => {
		// given
		const emptyNumberValue="";
		const mockReturnCartFromCSV = `Product name,Price,Quantity\nMollis consequat,9.00,${emptyNumberValue}\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": `Expected cell to be a positive number but received \"${emptyNumberValue}\"`
				})
			])
		);
	});
	it('should generate error about incorrect cell length, when second row has fewer fields (2 when expected 3)', () => {
		// given
		const mockReturnCartFromCSV = "Product name,Price,Quantity\nMollis consequat,9.00\nTvoluptatem,10.32,1";
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": "Expected row to have 3 cells but received 2."
				})
			])
		);
	});
	it('should generate error about empty string, when third row has empty string field', () => {
		// given
		const emptyStringValue = "";
		const mockReturnCartFromCSV = `Product name,Price,Quantity\nMollis consequat,9.00,2\n${emptyStringValue},10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": `Expected cell to be a nonempty string but received \"${emptyStringValue}\".`
				})
			])
		);
	});

	it('should generate error about incorrect number value, when second row has cell with negative number value', () => {
		// given
		const negativeNumberValue = "-56";
		const mockReturnCartFromCSV = `Product name,Price,Quantity\nMollis consequat,9.00,${negativeNumberValue}\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": `Expected cell to be a positive number but received "${negativeNumberValue}".`
				})
			])
		);
	});

	it('should generate error about incorrect number value, when second row has cell with not a type number', () => {
		// given
		const incorrectNumberValue = "somestring"; 
		const mockReturnCartFromCSV = `Product name,Price,Quantity\nMollis consequat,${incorrectNumberValue},2\nTvoluptatem,10.32,1`;
		//when
		const validated = parser.validate(mockReturnCartFromCSV);

		//then
		expect(validated).toEqual(expect.arrayContaining([
				expect.objectContaining({
					"message": `Expected cell to be a positive number but received "${incorrectNumberValue}".`
				})
			])
		);
	});

	it('should throw error, if error is found', () => {
		// given
		parser.readFile = jest.fn(function(){
			const incorrectNumberValue = "somestring";
			return `Product name,Price,Quantity\nMollis consequat,${incorrectNumberValue},2\nTvoluptatem,10.32,1`
		});

		//when
		const validated = () => { parser.parse(); }

		//then
		expect(validated).toThrowError(new Error('Validation failed!'));
	});

	it('should calculate total price correctly for correct data', () => {
		// given
		parser.readFile = jest.fn(function(){
			return `Product name,Price,Quantity\nMollis consequat,9.00,2\nTvoluptatem,10.32,1\nScelerisque lacinia,18.90,1\nConsectetur adipiscing,28.72,10\nCondimentum aliquet,13.90,1`
		});
		parser.calcTotal.bind(parser);

		//when
		const validated = parser.parse();

		//then
		expect(validated).toMatchObject( { total: 348.32 });
	});
	
});

describe('CartParser - integration test', () => {
	// Add your integration test here.
	it('should read content from cvs file', () => {
		const filePath = path.resolve(__dirname, '../samples/test.csv');
		const parsedText = parser.readFile(filePath);
		expect(parsedText).toBe('testhere');
	})
});
